<?
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\widgets\ListView;
?>
<? Pjax::begin(); ?>
<?= Html::beginForm(['site/form-submission'], 'post', ['data-pjax' => '', 'class' => 'form-inline']); ?>
    <?=
    ListView::widget([
        'dataProvider' => $model->getArrayDataProvider($model->getAllIngredients()),
        'layout' => '{items}',
        'options' => [
            'tag' => false
        ],
        'itemOptions' => [
            'tag' => false
        ],
        'itemView' => 'item'
    ])
    ?>
<?= Html::endForm() ?>

<? Pjax::end(); ?>