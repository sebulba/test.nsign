<?
use yii\helpers\Html;
?>

<?=
Html::submitButton(
    Html::img($model->getImagePath()),
    [
        'name' => 'id',
        'value' => $model->id
    ]
)
?>