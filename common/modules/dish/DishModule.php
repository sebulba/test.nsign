<?php

namespace common\modules\dish;

/**
 * dish module definition class
 */
class DishModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\dish\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->modules = [
            'ingredient' => [
                'class' => 'common\modules\dish\modules\ingredient\IngredientModule',
            ],
        ];

        $this->defaultRoute = 'dish';
    }
}
