<?php

namespace common\modules\dish\modules\ingredient\models;

use Yii;
use common\modules\dish\models\Dish;

/**
 * This is the model class for table "ingredient".
 *
 * @property integer $id
 * @property string $name
 * @property string $image
 * @property integer $status
 *
 * @property DishIngredient[] $dishIngredients
 * @property Dish[] $dishes
 */
class Ingredient extends \common\modules\dish\models\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ingredient';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['image'], 'file', 'extensions'=>'jpg, gif, png', 'maxSize' => 1024 * 1024],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'image' => 'Image',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDishIngredients()
    {
        return $this->hasMany(DishIngredient::className(), ['ingredient_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDishes()
    {
        return $this->hasMany(Dish::className(), ['id' => 'dish_id'])->viaTable('dish_ingredient', ['ingredient_id' => 'id']);
    }

    public function preparePath($extension)
    {
        return Yii::$app->params['uploadPathIngredient'] . $this->name . '.' .$extension;
    }
}
