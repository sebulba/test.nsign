<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\dish\modules\ingredient\models\Ingredient */

$this->title = 'Update Ingredient: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Ingredients', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ingredient-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->renderFile('@common/modules/dish/views/dish/_form.php', [
        'model' => $model,
    ]) ?>

</div>
