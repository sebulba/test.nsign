<?php

namespace common\modules\dish\modules\ingredient;

/**
 * ingredient module definition class
 */
class IngredientModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\dish\modules\ingredient\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->defaultRoute = 'ingredient';

        // custom initialization code goes here
    }
}
