<?php

namespace common\modules\dish\models;

use yii\data\ArrayDataProvider;
use yii\web\UploadedFile;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\data\ActiveDataProvider;

class ActiveRecord extends \yii\db\ActiveRecord
{
    public $imageInstance;

    public function uploadImage()
    {
        $image = UploadedFile::getInstance($this, 'image');

        if($image)
        {
            $this->_image = $image->name;
            $this->imageInstance = $image;
        }
        else
            $this->image = $this->getOldAttribute('image') . "";
    }

    public function set_Image($imageName)
    {
        $file_name = explode(".", $imageName);
        $ext = end($file_name);

        $path = $this->preparePath($ext);

        return $this->image = $path;
    }

    public function deleteImage() {
        if (empty($this->image) || !file_exists($this->image)) {
            return false;
        }

        if (!unlink($this->image)) {
            return false;
        }

        return true;
    }

    public function getOptions()
    {
        return [
            'options'=>['accept'=>'image/*'],
            'pluginOptions'=>[
                'allowedFileExtensions'=>['jpg','gif','png'],
                'previewFileType' => 'any',
                'initialPreview' => ($this->isNewRecord || empty($this->image)) ? false : Url::to($this->image, true),
                'initialPreviewAsData'=>true,
                'showRemove' => false,
                'showUpload' => false
            ]
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
                $this->uploadImage();
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($this->imageInstance)
        {
            Image::thumbnail($this->imageInstance->tempName, 100, 100)
                ->save($this->image, ['quality' => 80]);

            if(!$insert && isset($changedAttributes['image']))
                unlink($changedAttributes['image']);
        }
    }

    public function beforeDelete()
    {
        $this->deleteImage();
        return parent::beforeDelete();
    }

    public function getImagePath()
    {
        return Url::to($this->image, true);
    }

    public function getActiveDataProvider($query, $options = null)
    {
        $options['query'] = $query;

        return new ActiveDataProvider($options);
    }

    public function getArrayDataProvider($query, $options = null)
    {
        $options['allModels'] = $query;

        return new ArrayDataProvider($options);
    }

}
