<?php

use yii\db\Migration;

class m170304_153212_dish_ingredient extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dish_ingredient}}', [
            'id' => $this->primaryKey(),
            'dish_id' => $this->integer(),
            'ingredient_id' => $this->integer()
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%dish_ingredient}}');
    }
}
