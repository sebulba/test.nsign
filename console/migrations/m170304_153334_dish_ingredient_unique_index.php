<?php

use yii\db\Migration;

class m170304_153334_dish_ingredient_unique_index extends Migration
{
    public function up()
    {
        $this->createIndex(
            'iu_dish_ingredient$dish_id$ingredient_id',
            '{{%dish_ingredient}}',
            ['dish_id', 'ingredient_id'],
            true
        );
    }

    public function down()
    {
        $this->dropIndex('iu_dish_ingredient$dish_id$ingredient_id', '{{%dish_ingredient}}');
    }
}
