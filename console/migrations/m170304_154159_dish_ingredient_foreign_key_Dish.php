<?php

use yii\db\Migration;

class m170304_154159_dish_ingredient_foreign_key_Dish extends Migration
{
    public function up()
    {
        $this->addForeignKey(
            'fk_Dish__DishIngredient',
            '{{%dish_ingredient}}',
            'dish_id',
            '{{%dish}}',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk_Dish__DishIngredient', '{{%dish_ingredient}}');
    }
}
