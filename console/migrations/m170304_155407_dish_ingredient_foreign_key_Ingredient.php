<?php

use yii\db\Migration;

class m170304_155407_dish_ingredient_foreign_key_Ingredient extends Migration
{
    public function up()
    {
        $this->addForeignKey(
            'fk_Ingredient__DishIngredient',
            '{{%dish_ingredient}}',
            'ingredient_id',
            '{{%ingredient}}',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk_Ingredient__DishIngredient', '{{%dish_ingredient}}');
    }
}
